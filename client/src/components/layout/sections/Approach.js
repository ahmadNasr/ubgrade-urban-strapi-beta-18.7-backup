import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';
import {
  Container,
  Row,
  Col,
  Collapse,
  Card,
  CardBody,
  CollapseHeader
} from 'mdbreact';

class Approach extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse1: true,
      collapse2: false,
      collapse3: false,
      collapse4: false,
      collapse5: false
    };
  }

  toggleOne = collapseID => () => {
    this.setState({
      [collapseID]: !this.state[collapseID]
    });
  };

  render() {
    const {
      collapse1,
      collapse2,
      collapse3,
      collapse4,
      collapse5
    } = this.state;
    const { t } = this.props;

    return (
      <Container
        fluid
        className={`${t('language.isRTL') && 'text-right'}`}
        dir={`${t('language.isRTL') && 'rtl'}`}>
        <Row center className='urban-dark-orange z-depth-2'>
          <Col lg='7' md='8' sm='9' size='10'>
            <div className='text-white my-5 mx-2'>
              <h2 className='h2-responsive text-center font-weight-bold pb-4 text-uppercase'>
                {t('approach.paragraph.title')}
              </h2>
              <p
                className={`font-weight-normal pb-3 ${!t('language.isRTL') &&
                  'text-justify'}`}>
                {t('approach.paragraph.body.p1')}
              </p>
              <p className='font-weight-normal text-center pb-3'>
                {t('approach.paragraph.body.p2')}
              </p>
            </div>
          </Col>
        </Row>

        <Row center>
          <Col xl='6' lg='7' md='8' sm='7' size='9'>
            <div className='my-5 mx-2'>
              <h3 className='h3-responsive text-center pb-3'>
                {t('approach.steps.title')}
              </h3>
              <div className='text-white'>
                <Card className='border-0 z-depth-2'>
                  <CollapseHeader
                    className='urban-elegant-gray-10'
                    onClick={this.toggleOne('collapse1')}>
                    {t('approach.steps.p1.title')}
                    <i
                      className={
                        collapse1 ? 'fa fa-times' : 'fa fa-plus rotate-icon'
                      }
                    />
                  </CollapseHeader>
                  <Collapse
                    className='urban-elegant-gray-6 font-weight-light font-small'
                    id='collapse1'
                    isOpen={collapse1}>
                    <CardBody>{t('approach.steps.p1.body')}</CardBody>
                  </Collapse>
                </Card>

                <Card className='border-0 z-depth-2'>
                  <CollapseHeader
                    className='urban-elegant-gray-8'
                    onClick={this.toggleOne('collapse2')}>
                    {t('approach.steps.p2.title')}
                    <i
                      className={
                        collapse2 ? 'fa fa-times' : 'fa fa-plus rotate-icon'
                      }
                    />
                  </CollapseHeader>
                  <Collapse
                    id='collapse2'
                    className='urban-elegant-gray-6 font-weight-light font-small'
                    isOpen={collapse2}>
                    <CardBody>{t('approach.steps.p2.body')}</CardBody>
                  </Collapse>
                </Card>

                <Card className='border-0 z-depth-2'>
                  <CollapseHeader
                    className='grey darken-2'
                    onClick={this.toggleOne('collapse3')}>
                    {t('approach.steps.p3.title')}
                    <i
                      className={
                        collapse3 ? 'fa fa-times' : 'fa fa-plus rotate-icon'
                      }
                    />
                  </CollapseHeader>
                  <Collapse
                    id='collapse3'
                    className='urban-elegant-gray-6 font-weight-light font-small'
                    isOpen={collapse3}>
                    <CardBody>{t('approach.steps.p3.body')}</CardBody>
                  </Collapse>
                </Card>

                <Card className='border-0 z-depth-2'>
                  <CollapseHeader
                    className='grey darken-3'
                    onClick={this.toggleOne('collapse4')}>
                    {t('approach.steps.p4.title')}
                    <i
                      className={
                        collapse4 ? 'fa fa-times' : 'fa fa-plus rotate-icon'
                      }
                    />
                  </CollapseHeader>
                  <Collapse
                    id='collapse4'
                    className='urban-elegant-gray-6 font-weight-light font-small'
                    isOpen={collapse4}>
                    <CardBody>{t('approach.steps.p4.body')}</CardBody>
                  </Collapse>
                </Card>

                <Card className='border-0 z-depth-2'>
                  <CollapseHeader
                    className='bb-0 urban-elegant-gray-2'
                    onClick={this.toggleOne('collapse5')}>
                    {t('approach.steps.p5.title')}
                    <i
                      className={
                        collapse5 ? 'fa fa-times' : 'fa fa-plus rotate-icon'
                      }
                    />
                  </CollapseHeader>
                  <Collapse
                    id='collapse5'
                    className='urban-elegant-gray-6 font-weight-light font-small'
                    isOpen={collapse5}>
                    <CardBody>{t('approach.steps.p5.body')}</CardBody>
                  </Collapse>
                </Card>
              </div>
            </div>
          </Col>
        </Row>
      </Container>
    );
  }
}

export default withTranslation()(Approach);
