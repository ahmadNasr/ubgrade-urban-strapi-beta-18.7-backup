import React, { Component } from 'react';
import { HashRouter } from 'react-router-dom';
import { withTranslation } from 'react-i18next';
import {
  Navbar,
  NavbarBrand,
  NavbarNav,
  NavbarToggler,
  Collapse,
  NavItem,
  NavLink,
  Container,
  MDBBtn,
  toast
} from 'mdbreact';

import PropTypes from 'prop-types';
import { logoutUser } from '../../actions/authActions';
import { connect } from 'react-redux';
import { show } from 'redux-modal';
import ForgotPasswordModal from '../auth/ForgotPassword';
import AuthModal from '../auth';

import logo from '../../img/logo.png';

class NavbarFixed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
      isWideEnough: false
    };
  }

  handleScroll = () => {
    const scrollingNavbarOffset = 50;
    if (window.pageYOffset > scrollingNavbarOffset) {
      document.querySelector('#nav-logo').classList.remove('nav-logo');
      document.querySelector('#nav-logo').classList.add('nav-logo-small');
      document.querySelector('.navbar').classList.remove('z-depth-0');
      document.querySelector('.navbar').classList.add('z-depth-3');
    } else {
      document.querySelector('#nav-logo').classList.add('nav-logo');
      document.querySelector('#nav-logo').classList.remove('nav-logo-small');
    }
  };

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleOpen = name => () => {
    toast.dismiss();
    this.props.show(name);
  };

  onLogoutClick = e => {
    e.preventDefault();
    this.props.logoutUser();
  };

  onClick = () => {
    this.setState({
      collapse: !this.state.collapse
    });
  };
  render() {
    //const { isAuthenticated } = this.props.auth;
    const { t } = this.props;
    const authLink = (
      <NavItem onClick={this.onLogoutClick} className="py-1">
        <NavLink to="#" className="p-0 m-0" replace>
          <MDBBtn className="font-weight-500" outline size="sm" color="white">
            {t('forms.logout')}
          </MDBBtn>
        </NavLink>
      </NavItem>
    );
    const guestLink = (
      <NavItem onClick={this.handleOpen('authModal')} className="py-1">
        <NavLink to="#" className="p-0 m-0" replace>
          <MDBBtn className="font-weight-500" outline size="sm" color="white">
            {t('forms.login')}
          </MDBBtn>
        </NavLink>
      </NavItem>
    );
    return (
      <HashRouter hashType="noslash">
        <>
          <Navbar
            className={`z-depth-0 animated fadeInDown ${
              !t('language.isRTL') ? 'font-montserratEn' : 'font-montserratAr'
            }`}
            dark
            expand="md"
            fixed="top"
            color="light"
            scrolling
            dir={`${t('language.isRTL') && 'rtl'}`}
          >
            <Container className="px-0">
              <NavbarBrand
                href="/"
                className={`py-0 ${!t('language.isRTL') ? 'pl-3 pl-sm-0 mr-45' : 'pr-3 pr-sm-0 mr-0 ml-45'}`}
              >
                <img
                  src={logo}
                  id="nav-logo"
                  className="nav-logo d-inline-block py-2"
                  title="Urban Analysis Network | SYRIA"
                  alt="urban logo"
                />
              </NavbarBrand>

              {!this.state.isWideEnough && (
                <NavbarToggler className='px-3' onClick={this.onClick} />
              )}
              <Collapse isOpen={this.state.collapse} navbar>
                <NavbarNav
                  className='align-items-center font-weight-500 justify-content-around w-100 pr-0'
                >
                  <NavItem data-menuanchor="home" className="py-1" active>
                    <NavLink to="/home" replace>
                      {t('navbar.home')}
                    </NavLink>
                  </NavItem>
                  <NavItem data-menuanchor="approach" className="py-1">
                    <NavLink to="/approach" replace>
                      {t('approach.paragraph.title')}
                    </NavLink>
                  </NavItem>
                  <NavItem data-menuanchor="city-profiles" className="py-1">
                    <NavLink to="/city-profiles" replace>
                      {t('city-profiles.paragraph.title')}
                    </NavLink>
                  </NavItem>
                  <NavItem data-menuanchor="toolkit" className="py-1">
                    <NavLink to="/toolkit" replace>
                      {t('toolkit.paragraph.title')}
                    </NavLink>
                  </NavItem>
                  <NavItem data-menuanchor="about" className="py-1">
                    <NavLink to="/about" replace>
                      {t('about.paragraph.title')}
                    </NavLink>
                  </NavItem>
                  <NavItem data-menuanchor="contact" className="py-1">
                    <NavLink to="/contact" replace>
                      {t('contact.title')}
                    </NavLink>
                  </NavItem>
                  {localStorage.jwtToken ? authLink : guestLink}
                </NavbarNav>
              </Collapse>
            </Container>
          </Navbar>
          <AuthModal /> <ForgotPasswordModal />
        </>
      </HashRouter>
    );
  }
}

NavbarFixed.propTypes = {
  logoutUser: PropTypes.func.isRequired,

  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { show, logoutUser }
)(withTranslation()(NavbarFixed));
